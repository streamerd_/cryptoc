defmodule Streamer.Binance do
  use WebSockex

  @stream_endpoint "wss://stream.binance.com:9443/ws/"

  # Example start_links
  # iex> Streamer.Binance.start_link("xrpustd", [])    => XRP binance.

  def start_link(symbol, state) do
    WebSockex.start_link(
      "#{@stream_endpoint}#{symbol}@trade",
      __MODULE__,
      state
    )
  end

  def handle_frame({_type, msg}, state) do
    case Jason.decode(msg) do
      {:ok, event} -> handle_event(event, state)
      {:error, _} -> throw("Unable to parse the msg")
    end

    {:ok, state}
  end

  # Make sure received event is casted to the Binance.TradeEvent struct.
  # https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md#trade-streams


  def handle_event(%{"e" => "trade"} = event, _state) do
    trade_event = %Streamer.Binance.TradeEvent{
      :event_type => event["e"],
      :event_time => event["E"],
      :symbol => event["s"],
      :trade_id => event["t"],
      :price => event["p"],
      :quantity => event["q"],
      :buyer_order_id => event["b"],
      :seller_order_id => event["a"],
      :trade_time => event["T"],
      :buyer_market_maker => event["m"]
    }

    IO.inspect(trade_event, label: "Trade event received: ")
  end
end
