# Cryptoc

**Consuming Binance real-time cryptocurrency trading events through websocket | decoding and casting to a corresponding struct**

Developed with:
-----------------
erlang 23.0  
elixir 1.10.0
-----------------

Actual trading data streamer project created as a supervised application(streamer) within an umbrella application(cryptoc)'s /apps folder.

    $ <now you are at the project root>
    $ cd apps/streamer
    $ mix deps.get  
    $ iex -S mix  
    $ iex> Streamer.Binance.start_link("xrpusdt", [])
    .
    .
  
   ![result](https://gitlab.com/streamerd_/cryptoc/-/blob/master/binance_streamer_result.png)